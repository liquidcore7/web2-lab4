import * as express from 'express';
import { Application } from 'express';
import { AddressInfo } from 'net';
import { json } from 'body-parser';
import * as cors from 'cors';
import { IBindable } from './controller/bindable';


export default class App {
    private readonly expressApp: Application;
    public readonly port: number;
    public readonly host: string;

    constructor(port: number,
                host: string,
                controllers: IBindable[]) {
        this.port = port;
        this.host = host;
        this.expressApp = express();

        this.expressApp.use(cors());
        this.expressApp.use(json());

        controllers.forEach( controller => controller.bindToApp(this.expressApp) );
    }


    serveForever() {
        const listener = this.expressApp.listen(this.port, this.host).address() as AddressInfo;
        console.log(
            `Express app started on ${listener.address}:${listener.port}`
        );
    }
};