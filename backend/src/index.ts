import App from './app';
import { CrudController } from './controller/crud_controller';
import { MongoRepo } from './repo/mongo_repo';
import { ICustomer, Customer } from './domain/customer';
import { CustomersCollectionModel } from './repo/models/customer';
import { IExecutor, Executor } from './domain/executor';
import { ExecutorsCollectionModel } from './repo/models/executor';
import { IProject, Project } from './domain/project';
import { ProjectsCollectionModel } from './repo/models/project';
import { IExecution, Execution } from './domain/execution';
import { ExecutionsCollectionModel } from './repo/models/execution';
import { connect } from 'mongoose';
import { FilteringMiddleware, EndpointType } from './controller/middleware';
import * as yargs from 'yargs';
import { ReportController } from './reportcontrollers/report_controller';
import { ObjectId } from 'mongodb';

const argv = yargs
    .option('port', {
        alias: 'p',
        description: 'Port to start server on',
        type: 'number',
        default: 3000
    })
    .option('host', {
        alias: 'h',
        description: 'Hostname to start server on',
        type: 'string',
        default: 'localhost'
    })
    .option('mongoHost', {
        alias: 'm',
        description: 'Host for MongoDB',
        type: 'string',
        default: 'localhost'
    })
    .help()
    .alias('help', 'h')
    .argv;

const repos = {
    customer: new MongoRepo<ICustomer>(CustomersCollectionModel, [
        Customer('Andrii', 20_000),
        Customer('NU LP', 999_999),
        Customer('LNMU medical university', 1_550_000)
    ]),
    executor: new MongoRepo<IExecutor>(ExecutorsCollectionModel, [
        Executor('SoftServe Inc.', 20, 1300),
        Executor('EPAM', 25, 3000),
        Executor('JetSoftPro', 5, 100)
    ]),
    project: new MongoRepo<IProject>(ProjectsCollectionModel, [
        Project('Healthcare platform', new ObjectId().toHexString()),
        Project('Virtual educational center', new ObjectId().toHexString(), 'A scalable platform for MOOCs')
    ]),
    execution: new MongoRepo<IExecution>(ExecutionsCollectionModel, [
        Execution(new ObjectId().toHexString(), new ObjectId().toHexString(), new Date('2014-01-04'), new Date('2020-01-01')),
        Execution(new ObjectId().toHexString(), new ObjectId().toHexString(), new Date('2018-05-11'))
    ])
}


const application = new App(
    Number(argv.port).valueOf() || 3000,
    argv.host,
    [
        new CrudController('/customers', repos.customer),
        new CrudController('/executors', repos.executor),
        new CrudController('/projects', repos.project),
        new CrudController('/executions', repos.execution)
            // .attachMiddleware(new FilteringMiddleware(
            //     [EndpointType.Create, EndpointType.Update],
            //     (req, repo) => {
            //         const body = req.body as IExecution;
            //         return repo.fetchAll()
            //             .then(executions => {
            //                 const conflicting = executions.map(e => e.body)
            //                     .filter(e => (e.startDate.getMilliseconds() < body.startDate.getMilliseconds()) && 
            //                                  (e.endDate.getMilliseconds() > body.startDate.getMilliseconds())
            //                     );
            //                 console.log(conflicting.length);
            //                 if (conflicting.length > 3) return Promise.reject('Too many executions during the period');
            //                 else return Promise.resolve(req);
            //             });
            //     }
            // )),
            ,
        new ReportController('/executionsByExecutor', repos.execution, exec => exec.executorId.toString()),
        new ReportController('/projectsByCustomer', repos.project, proj => proj.customerId.toString())
    ]
);

connect(`mongodb://${argv.mongoHost}:27017`, { useNewUrlParser: true, user: 'root', pass: 'rootpassword', useUnifiedTopology: true })
    .then(_ => application.serveForever())
    .then(_ => setTimeout(async _ => await CustomersCollectionModel
        .find()
        .exec()
        .then(res => res[0] as ICustomer)
        .then(async res => await ProjectsCollectionModel.updateMany({}, {$set: {customerId: res.id}}).exec())
    , 14000))