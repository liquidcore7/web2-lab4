import { IWithID, Entity } from "../domain/entity";
import { Model, Document } from "mongoose";
import { Request, Response, Application } from "express";
import { EntitiesJsonResponse, IIdParams, ListResponse, ResponseWithError } from '../controller/crud_controller';
import { IBindable } from '../controller/bindable';
import { filter } from 'lodash/fp';
import { ICrudRepo } from "../repo/crud_repo";


export class ReportController<T extends IWithID<any>> implements IBindable {
    private readonly path: string;
    private readonly repo: ICrudRepo<any, T>;
    private readonly lens: (fromModel: T) => string;

    constructor(path: string,
                repo: ICrudRepo<any, T>,
                lens: (fromModel: T) => string) {
                    this.path = path;
                    this.repo = repo;
                    this.lens = lens;
                }

    findAllByField = (req: Request<IIdParams>, res: Response): Promise<EntitiesJsonResponse<T>> => {
        const requestedValue = req.params.id;

        return this.repo.fetchAll()
                    .then(filter(e => this.lens(e.body) === requestedValue))
                    .then(resp => new ListResponse(resp))
                    .then(
                        entities => res.json(ResponseWithError.right(entities)),
                        err => res.json(ResponseWithError.left(err.message))
                    );
    }
                

    bindToApp = (app: Application): Application =>
        app.get(this.path + '/:id', this.findAllByField);
 
}