import { ICrudRepo } from "../repo/crud_repo";
import { Entity, IWithID } from "../domain/entity";
import { Request, Response, Application } from "express";
import { ParamsDictionary } from 'express-serve-static-core';
import { FilteringMiddleware, EndpointType } from "./middleware";
import * as _ from 'lodash';
import { IBindable } from "./bindable";


export type EntityJsonResponse<I> = Response<ResponseWithError<EntityResponse<any, I>>>;
export type EntitiesJsonResponse<I> = Response<ResponseWithError<ListResponse<any, I>>>;
export type ResponseP<I> = Promise<EntityJsonResponse<I> | EntitiesJsonResponse<I> | Response<ResponseWithError<{}>>>
export type Endpoint<P extends ParamsDictionary, I> = (req: Request<P>, res: Response) => ResponseP<I>;


export class CrudController<I extends IWithID<any>> implements IBindable {
    private readonly route: string;
    private readonly crudRepo: ICrudRepo<any, I>;
    private middlewares: FilteringMiddleware<I, any>[] = [];

    constructor(
        route: string, 
        crudRepo: ICrudRepo<any, I>) {
            this.route = route;
            this.crudRepo = crudRepo; 
    }

    private preFilter = <P extends ParamsDictionary>(
        endpointType: EndpointType, 
        action: Endpoint<P, I>
    ): Endpoint<P, I> => (req: Request<P>, res: Response): ResponseP<I> => 
        this.middlewares
            .filter(m => _.includes(m.endpointTypes, endpointType))
            .reduce(
                (req, mws) => req.then(async newReq => await mws.requestFilter(newReq, this.crudRepo)), 
                Promise.resolve(req)
            ).then(
                async filteredReq => await action(filteredReq, res),
                error => res.json(ResponseWithError.left(error))
            );

    attachMiddleware = (m: FilteringMiddleware<I, IIdParams> | FilteringMiddleware<I, ParamsDictionary>): CrudController<I> => {
        this.middlewares.push(m);
        return this;
    }

    getById = (req: Request<IIdParams>, res: Response): Promise<EntityJsonResponse<I>> =>
        this.crudRepo
            .findById(req.params.id)
            .then(entity => res.json(entity === null ? 
                                     ResponseWithError.left(`Entity with id=${req.params.id} not found`) :
                                     ResponseWithError.right(new EntityResponse(entity))
                                    )
                 );


    create = (req: Request, res: Response): Promise<EntityJsonResponse<I>> =>
        this.crudRepo
            .create( req.body as I )
            .then(entity => res.json(entity === null ? 
                                     ResponseWithError.left(`Failed to create entity, invalid request body.`) :
                                     ResponseWithError.right(new EntityResponse(entity))
                                    ),
                   err => res.json(ResponseWithError.left(err.message))
                 );


    list = (req: Request, res: Response): Promise<EntitiesJsonResponse<I>> => 
        this.crudRepo
            .fetchAll()
            .then(all => res.json(ResponseWithError.right(new ListResponse(all))));        


    delete = (req: Request<IIdParams>, res: Response): Promise<Response<ResponseWithError<{}>>> =>
        this.crudRepo
            .deleteById(req.params.id)
            .then(
                _ => res.json(ResponseWithError.right({})),
                err => res.json(ResponseWithError.left(`Failed to delete entity with ID=${req.params.id}: ${err}`))
            );


    update = (req: Request<IIdParams>, res: Response): Promise<EntityJsonResponse<I>> =>
        this.crudRepo
            .update(req.params.id, req.body as I)
            .then(
                entity => res.json(entity === null ? 
                                     ResponseWithError.left(`Failed to create entity, invalid request body.`) :
                                     ResponseWithError.right(new EntityResponse(entity))
                                    ),
                err => res.json(ResponseWithError.left(`No entity with ID=${req.params.id} found: ${err}`))
            );


    bindToApp = (app: Application): Application =>
        app.post(this.route, this.preFilter(EndpointType.Create, this.create))
           .put(this.route + '/:id', this.preFilter(EndpointType.Update, this.update))
           .delete(this.route + '/:id', this.preFilter(EndpointType.Delete, this.delete))
           .get(this.route + '/:id', this.preFilter(EndpointType.GetById, this.getById))
           .get(this.route, this.preFilter(EndpointType.List, this.list));
};


export class ResponseWithError<BoxedResponse> {
    public readonly error?: string;
    public readonly body?: BoxedResponse;

    constructor(err?: string, body?: BoxedResponse) {
        this.error = err;
        this.body = body;
    }

    static right = <R>(body: R): ResponseWithError<R> => new ResponseWithError(undefined, body);
    static left  = (error: string): ResponseWithError<Response<{}>> => new ResponseWithError<Response<{}>>(error, undefined);
}


export interface IIdParams extends ParamsDictionary {
    id: string; 
};

export class ListResponse<Id, I extends IWithID<Id>> {
    public readonly entities: I[];

    constructor(entities: Entity<Id, I>[]) {
        this.entities = entities.map(e => e.body);
    }
};

export class EntityResponse<Id, I extends IWithID<Id>> {
    public readonly entity: I | null;

    constructor(entity: Entity<Id, I> | null) {
        this.entity = entity ? entity.body : null;
    }
}
