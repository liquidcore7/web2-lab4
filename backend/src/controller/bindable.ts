import { Application } from "express";

export interface IBindable {
    bindToApp(app: Application): Application;
}