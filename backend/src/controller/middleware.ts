import { ICrudRepo } from '../repo/crud_repo';
import { Request } from "express";
import { ParamsDictionary } from 'express-serve-static-core';
import { IWithID } from '../domain/entity';


export enum EndpointType {
    GetById, List, Create, Update, Delete
};

export type Filter<I extends IWithID<any>, P extends ParamsDictionary> = (req: Request<P>, repo: ICrudRepo<any, I>) => Promise<Request<P>>;


export class FilteringMiddleware<I extends IWithID<any>, P extends ParamsDictionary> {
    readonly endpointTypes: EndpointType[];
    readonly requestFilter: Filter<I, P>;

    constructor(eps: EndpointType[], filter: Filter<I, P>) {
        this.endpointTypes = eps;
        this.requestFilter = filter;
    }
};