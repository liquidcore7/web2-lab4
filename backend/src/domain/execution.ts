import { IWithID } from "./entity";

export interface IExecution extends IWithID<any> {
    readonly projectId: string;
    readonly executorId: string;
    readonly startDate: Date;
    readonly endDate: Date;
};

export const Execution = (projectId: string, executorId: string, startDate?: Date, endDate?: Date): IExecution => ({
    projectId: projectId,
    executorId: executorId,
    startDate: startDate == undefined ? new Date() : startDate as Date,
    endDate: endDate == undefined ? new Date() : endDate as Date
});