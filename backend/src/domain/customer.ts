import { Entity, IWithID } from './entity';


export interface ICustomer extends IWithID<any> {
  readonly name: string;
  readonly budget: number;
}

export const Customer = (name: string, budget: number): ICustomer => ({
  name: name,
  budget: budget
});