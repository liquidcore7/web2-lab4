import { Entity, IWithID } from './entity';

export interface IProject extends IWithID<any> {
  readonly name: string;
  readonly description?: string;
  readonly customerId: string;
};

export const Project = (name: string, customerId: string, description?: string): IProject => ({
  name: name,
  description: description,
  customerId: customerId
});
