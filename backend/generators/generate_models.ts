import { ASTHelper } from './generator_commons';
import { InterfaceDeclaration, SourceFile, VariableDeclarationKind, VariableStatement } from 'ts-morph';
import { join, capitalize } from 'lodash';

const AstHelper = new ASTHelper();


const generateMongoDocumentInterface = (sourceFile: SourceFile, iEntity: InterfaceDeclaration): InterfaceDeclaration =>
    sourceFile.addInterface({
        name: iEntity.getName() + 'MongoDocument',
        extends: [iEntity.getName(), 'mongoose.Document'],
        // properties: iEntity.getProperties().map(prop => ({
        //     name: prop.getName(),
        //     isReadonly: prop.isReadonly(),
        //     type: prop.getType().getText(),
        //     hasQuestionToken: prop.hasQuestionToken()
        // }))
    });

const generateMongoSchema = (sourceFile: SourceFile, iEntity: InterfaceDeclaration): VariableStatement => {
    const schemaDefName = iEntity.getName() + 'Schema';

    const schemaDefBody = '{\n' + join(iEntity.getProperties().map(prop => 
        `${prop.getName()}: { type: ${capitalize(prop.getType().getText())}, required: ${!prop.hasQuestionToken()} }`
    ), ',\n') + '\n}';

    return sourceFile.addVariableStatement({
        declarationKind: VariableDeclarationKind.Const,
        declarations: [{
            name: schemaDefName,
            initializer: `new mongoose.Schema(${schemaDefBody})`
        }]
    });
}

const generateMongoModel = (sourceFile: SourceFile, 
                            entityName: String, 
                            documentIface: InterfaceDeclaration, 
                            schema: VariableStatement): VariableStatement => {
    const modelName = entityName + 'Model';

    return sourceFile.addVariableStatement({
        declarationKind: VariableDeclarationKind.Const,
        isExported: true,
        declarations: [{
            name: modelName,
            initializer: `mongoose.model<${documentIface.getName()}>("${modelName}", ${schema.getDeclarations()[0].getName()})`
        }]
    });
};
    

const generateMongoModels = (iEntity: InterfaceDeclaration): void => {
    const entityName = iEntity.getName().substring(1).toLowerCase();
    const fileName = `src/repo/models/${entityName}.ts`;

    console.info(`Writing ${fileName}:`);

    const sourceFile = AstHelper.ProjectInstance.createSourceFile(
        fileName,
        `import * as mongoose from "mongoose";\n` + 
        `import { ${iEntity.getName()} } from '../../domain/${entityName}';`,
        {
            overwrite: true
        }
    );

    console.info("Creating interface extending Document (1/3)");
    const IDocument = generateMongoDocumentInterface(sourceFile, iEntity);

    console.info("Creating schema definition (2/3)");
    const schemaDef = generateMongoSchema(sourceFile, iEntity);

    console.info("Creating model (3/3)");
    const modelDef  = generateMongoModel(sourceFile, iEntity.getName().substring(1) + 'sCollection', IDocument, schemaDef); 
};


console.info(`Generating mongo models for ${AstHelper.IWithId.getName()} descendants...`);
AstHelper.traverse(generateMongoModels);
