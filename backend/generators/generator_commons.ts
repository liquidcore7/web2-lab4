import { Project, SourceFile, InterfaceDeclaration, SyntaxKind, PropertySignature } from 'ts-morph';
import { takeWhile } from 'lodash'


export class ASTHelper {
  public readonly ProjectInstance: Project;
  public readonly IWithId: InterfaceDeclaration;
  public readonly IEntities: InterfaceDeclaration[];

  public static degenerify = (className: string): string =>
    takeWhile(className, ch => ch != '<').join('');

  constructor() {
    this.ProjectInstance = new Project({
      addFilesFromTsConfig: false
    });
    const childInterfacePaths = this.ProjectInstance.addSourceFilesAtPaths('src/domain/*.ts');
    const entityBaseFile: SourceFile = this.ProjectInstance.getSourceFileOrThrow('src/domain/entity.ts');
    this.IWithId = entityBaseFile.getInterfaceOrThrow('IWithID');
    this.IEntities = childInterfacePaths
      .map(path => path.getStatementByKind(SyntaxKind.InterfaceDeclaration))
      .filter(iface => iface != undefined)
      .map(iface => iface as InterfaceDeclaration)
      .filter(iface => {
        const parents = iface
          .getExtends()
          .map(e => e.getText())
          .map(ASTHelper.degenerify);

        return parents.indexOf(ASTHelper.degenerify(this.IWithId.getName())) >= 0;
      });
  }


  traverse = (ifaceConsumer: (iEntity: InterfaceDeclaration) => void): void => {
    this.IEntities.forEach(ifaceConsumer);
    this.ProjectInstance.saveSync();
  }
}
