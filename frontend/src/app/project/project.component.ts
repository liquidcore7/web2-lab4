import {Component, Inject, OnInit} from '@angular/core';
import {AbstractEntityComponent, IDialogData} from '../abstract-entity-component';
import {ICustomer, Customer} from '../domain/customer';
import {HttpWebService} from '../http-web.service';
import {FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {IProject, Project} from '../domain/project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css'],
  providers: [HttpWebService]
})
export class ProjectComponent extends AbstractEntityComponent<IProject> implements OnInit {

  public createForm;

  constructor(httpWebService: HttpWebService,
              snackBar: MatSnackBar,
              dialogR: MatDialog,
              private formBuilder: FormBuilder) {
    super('projects', httpWebService, snackBar, dialogR);

    this.createForm = this.formBuilder.group({
        name: '',
        description: '',
        customerId: 0
      });
  }

  async updateEntry(entity: IProject) {
    const dialogRef = this.dialog.open(ProjectUpdateDialogComponent, {
      height: '400px',
      data: {action: 'update', body: entity}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.update(result._id, result));
  }

  async createEntry() {
    const dialogRef = this.dialog.open(ProjectUpdateDialogComponent, {
      height: '400px',
      data: {action: 'create', body: Project('', '', '')}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.create(result));
  }



  ngOnInit(): void {

  }

}


@Component({
  selector: 'app-project-update-dialog',
  templateUrl: 'project.update.dialog.html',
})
export class ProjectUpdateDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ProjectUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData<IProject>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
