import {IWithID} from './domain/entity';
import {HttpWebService} from './http-web.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog} from '@angular/material/dialog';


export abstract class AbstractEntityComponent<I extends IWithID<any>> {
  private allEntities: I[] = [];


  protected constructor(entityUrlPath: string,
                        private httpWebService: HttpWebService,
                        protected snackBar: MatSnackBar,
                        public dialog: MatDialog) {
    this.httpWebService.setEntityUrlPath(entityUrlPath);
    this.refetch();
  }

  private async refetch() {
    this.allEntities = await this.list();
  }

  findById(id: any): Promise<I> {
    return this.httpWebService
      .fetchById<I>(id)
      .toPromise()
      .then(res => res.body?.entity);
  }


  create(entity: I): Promise<void> {
    return this.httpWebService.create(entity).forEach(
      res => res.error !== undefined ? this.showErrorMessage(res.error) : {}
    ).then(
      _ => this.refetch(),
      err => this.showErrorMessage(err.message)
    );
  }

  list(): Promise<I[]> {
    return this.httpWebService
      .fetchAll<I>()
      .toPromise()
      .then(res => {
        if (res.error !== undefined) {
          this.showErrorMessage(res.error);
          return [];
        } else {
          return res.body.entities;
        }
      }, err => { this.showErrorMessage(err.message); return []; });
  }

  update(id: any, newEntity: I): Promise<void> {
    return this.httpWebService
      .update(id, newEntity)
      .toPromise()
      .then(res => {
        if (res.error !== undefined) {
          this.showErrorMessage(res.error);
        }
      })
      .then(_ => this.refetch())
      .catch(err => this.showErrorMessage(err.message));
  }

  delete(id: any): Promise<void> {
    return this.httpWebService
      .delete(id)
      .toPromise()
      .then(res => {
        if (res.error !== undefined) {
          this.showErrorMessage(res.error);
        }
      })
      .then(_ => this.refetch())
      .catch(err => this.showErrorMessage(err.message));
  }

  getElements(): I[] {
    return this.allEntities;
  }

  protected showErrorMessage(msg: string): void {
    this.snackBar.open(msg, 'x', {
      duration: 4000,
    });
  }
}

export interface IDialogData<I> {
  readonly action: string;
  body: I;
}
