import {Component, Inject, OnInit} from '@angular/core';
import {AbstractEntityComponent, IDialogData} from '../abstract-entity-component';
import {ICustomer, Customer} from '../domain/customer';
import {HttpWebService} from '../http-web.service';
import {FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Execution, IExecution} from '../domain/execution';

@Component({
  selector: 'app-execution',
  templateUrl: './execution.component.html',
  styleUrls: ['./execution.component.css'],
  providers: [HttpWebService]
})
export class ExecutionComponent extends AbstractEntityComponent<IExecution> implements OnInit {

  public createForm;

  constructor(httpWebService: HttpWebService,
              snackBar: MatSnackBar,
              dialogR: MatDialog,
              private formBuilder: FormBuilder) {
    super('executions', httpWebService, snackBar, dialogR);

    this.createForm = this.formBuilder.group({
        projectId: '',
        executorId: '',
        startDate: new Date(),
        endDate: new Date()
      });
  }

  async updateEntry(entity: IExecution) {
    const dialogRef = this.dialog.open(ExecutionUpdateDialogComponent, {
      height: '460px',
      data: {action: 'update', body: entity}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.update(result._id, result));
  }

  async createEntry() {
    const dialogRef = this.dialog.open(ExecutionUpdateDialogComponent, {
      height: '460px',
      data: {action: 'create', body: Execution('', '', new Date(), new Date())}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.create(result));
  }



  ngOnInit(): void {

  }

}


@Component({
  selector: 'app-execution-update-dialog',
  templateUrl: 'execution.update.dialog.html',
})
export class ExecutionUpdateDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ExecutionUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData<IExecution>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
