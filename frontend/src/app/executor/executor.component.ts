import {Component, Inject, OnInit} from '@angular/core';
import {AbstractEntityComponent, IDialogData} from '../abstract-entity-component';
import {HttpWebService} from '../http-web.service';
import {FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {Executor, IExecutor} from '../domain/executor';

@Component({
  selector: 'app-executor',
  templateUrl: './executor.component.html',
  styleUrls: ['./executor.component.css'],
  providers: [HttpWebService]
})
export class ExecutorComponent extends AbstractEntityComponent<IExecutor> implements OnInit {

  public createForm;

  constructor(httpWebService: HttpWebService,
              snackBar: MatSnackBar,
              dialogR: MatDialog,
              private formBuilder: FormBuilder) {
    super('executors', httpWebService, snackBar, dialogR);

    this.createForm = this.formBuilder.group({
        name: '',
        experience: 0,
        employees: 0
      });
  }

  async updateEntry(entity: IExecutor) {
    const dialogRef = this.dialog.open(ExecutorUpdateDialogComponent, {
      height: '400px',
      data: {action: 'update', body: entity}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.update(result._id, result));
  }

  async createEntry() {
    const dialogRef = this.dialog.open(ExecutorUpdateDialogComponent, {
      height: '400px',
      data: {action: 'create', body: Executor('', 0, 0)}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.create(result));
  }



  ngOnInit(): void {

  }

}


@Component({
  selector: 'app-executor-update-dialog',
  templateUrl: 'executor.update.dialog.html',
})
export class ExecutorUpdateDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<ExecutorUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData<IExecutor>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
