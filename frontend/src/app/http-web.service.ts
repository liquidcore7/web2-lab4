import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IWithID} from './domain/entity';
import {MultiResponse, SingleResponse, VoidResponse} from './domain/response';
import {environment} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpWebService {

  private readonly baseUrl: string = environment.baseUrl;
  private entityUrlPath: string;

  constructor(private http: HttpClient) { }

  setEntityUrlPath(to: string) {
    this.entityUrlPath = to;
  }


  fetchAll<I extends IWithID<any>>(): Observable<MultiResponse<I>> {
    return this.http
        .get<MultiResponse<I>>(this.baseUrl + this.entityUrlPath, {observe: 'body', responseType: 'json'});
  }

  fetchById<I extends IWithID<any>>(id: any): Observable<SingleResponse<I>> {
    return this.http
      .get<SingleResponse<I>>(this.baseUrl + this.entityUrlPath + `/${id}`, {observe: 'body', responseType: 'json'});
  }

  create<I extends IWithID<any>>(entity: I): Observable<SingleResponse<I>> {
    return this.http
      .post<SingleResponse<I>>(this.baseUrl + this.entityUrlPath, entity, {observe: 'body', responseType: 'json'});
  }

  update<I extends IWithID<any>>(id: any, entity: I): Observable<SingleResponse<I>> {
    return this.http
      .put<SingleResponse<I>>(this.baseUrl + this.entityUrlPath + `/${id}`, entity, {observe: 'body', responseType: 'json'});
  }

  delete(id: any): Observable<VoidResponse> {
    return this.http
      .delete<VoidResponse>(this.baseUrl + this.entityUrlPath + `/${id}`, {observe: 'body', responseType: 'json'});
  }


}
