import { IWithID } from './entity';


export interface IExecution extends IWithID<any> {
    projectId: string;
    executorId: string;
    startDate: Date;
    endDate: Date;
}

export const Execution = (projectId: string, executorId: string, startDate?: Date, endDate?: Date): IExecution => ({
    projectId,
    executorId,
    startDate: startDate === undefined ? new Date() : startDate as Date,
    endDate: endDate === undefined ? new Date() : endDate as Date
});
