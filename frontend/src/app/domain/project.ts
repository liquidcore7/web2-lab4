import { IWithID } from './entity';


export interface IProject extends IWithID<any> {
  name: string;
  description?: string;
  customerId: string;
}

export const Project = (name: string, customerId: string, description?: string): IProject => ({
  name,
  description,
  customerId
});
