import {IWithID} from './entity';


export interface SingleEntity<I extends IWithID<any>> {
  readonly entity: I;
}

export interface MultipleEntities<I extends IWithID<any>> {
  readonly entities: I[];
}



export class SingleResponse<I extends IWithID<any>> {
  readonly error?: string;
  readonly body?: SingleEntity<I>;
}

export class MultiResponse<I extends IWithID<any>> {
  readonly error?: string;
  readonly body?: MultipleEntities<I>;
}

export class VoidResponse {
  readonly error?: string;
  readonly body?: {};
}
