export interface IWithID<Id> {
  _id?: Id;
}


export class Entity<Id, T extends IWithID<Id>> {
  public readonly body: T;

  constructor(body: T) {
    this.body = body;
  }

  withId(newId: Id): Entity<Id, T> {
    this.body._id = newId;
    return this;
  }
}
