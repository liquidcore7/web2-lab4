import { IWithID } from './entity';


export interface IExecutor extends IWithID<any> {
    name: string;
    experience: number;
    employees: number;
}

export const Executor = (name: string, experience: number, employees: number): IExecutor => ({
    name,
    experience,
    employees
});
