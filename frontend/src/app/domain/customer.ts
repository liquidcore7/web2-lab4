import { Entity, IWithID } from './entity';


export interface ICustomer extends IWithID<any> {
  name: string;
  budget: number;
}

export const Customer = (name: string, budget: number): ICustomer => ({
  name,
  budget
});
