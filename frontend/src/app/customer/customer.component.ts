import {Component, Inject, OnInit} from '@angular/core';
import {AbstractEntityComponent, IDialogData} from '../abstract-entity-component';
import {ICustomer, Customer} from '../domain/customer';
import {HttpWebService} from '../http-web.service';
import {FormBuilder} from '@angular/forms';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {IProject} from '../domain/project';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {MultiResponse} from '../domain/response';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css'],
  providers: [HttpWebService]
})
export class CustomerComponent extends AbstractEntityComponent<ICustomer> implements OnInit {

  public createForm;
  public selectedId?: any;
  private activeProjects: IProject[] = [];

  constructor(httpWebService: HttpWebService,
              snackBar: MatSnackBar,
              dialogR: MatDialog,
              private formBuilder: FormBuilder,
              private webClient: HttpClient) {
    super('customers', httpWebService, snackBar, dialogR);

    this.createForm = this.formBuilder.group({
        name: '',
        budget: 0
      });
  }

  async onSelect(id: any) {
    this.onUnSelect();
    await this.webClient
      .get<MultiResponse<IProject>>(environment.baseUrl + `projectsByCustomer/${id}`)
      .forEach(response => {
        if (response.error !== undefined) {
          this.showErrorMessage(response.error);
        } else {
          this.activeProjects = response.body.entities;
        }
      });
  }
  onUnSelect() {
    this.activeProjects = [];
  }

  getActiveProjects() {
    return this.activeProjects;
  }

  async updateEntry(entity: ICustomer) {
    const dialogRef = this.dialog.open(CustomerUpdateDialogComponent, {
      height: '400px',
      data: {action: 'update', body: entity}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.update(result._id, result));
  }

  async createEntry() {
    const dialogRef = this.dialog.open(CustomerUpdateDialogComponent, {
      height: '400px',
      data: {action: 'create', body: Customer('', 0)}
    });

    await dialogRef.afterClosed()
      .toPromise()
      .then(result => this.create(result));
  }



  ngOnInit(): void {

  }

}


@Component({
  selector: 'app-customer-update-dialog',
  templateUrl: 'customer.update.dialog.html',
})
export class CustomerUpdateDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<CustomerUpdateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogData<ICustomer>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
